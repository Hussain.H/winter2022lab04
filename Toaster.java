import java.util.Scanner;
public class Toaster{
	
	private String noise;
	private String color;
	private int power;
	
	//Adding a constructor
	public Toaster(String noise, String color, int power){
		this.noise = noise;
		this.color = color;
		this.power =power;
		
	}
	
	
	public void ding(){
		System.out.println("a toaster should go DINGGGGG");
	}
	
	public void toast(){
		if(this.power<=1000){
			System.out.println("perfect well made bread :)");
		}
		else {
			System.out.println("just a tiny bit too hot for toast (sarcasm :|)");
		}
	}
	
	public void powerUpValue(int userinput){
		
		int validValue = validateInput(userinput);
		this.power = this.power+validValue;
		
	}
	
	
	public int validateInput(int input){
		Scanner sc = new Scanner(System.in);
		
		if(input > 0){
			return input;
		}
		do {
			System.out.println("No.. Give me positive number...");
			input = sc.nextInt();
	
		
		
		}
		while(input<0);
		return input;
	}
	
	/*
	public void powerUp(int extraPower){
		if (extraPower <= 0){
		this.power= this.power + extraPower;
		}
		else{
			System.out.println("I don't think you can go lower with that toaster :/")
		}
	*/
	
	
	
	
	
	
	//Setters
	
	public void setNoise(String newNoise){
		this.noise = newNoise;
	}
	/*
	public void setColor(String newColor){
		this.color= newColor;
	}
	*/
	public void setPower(int newPower){
		this.power=newPower;
	}
	
	//Getters
	public String getNoise(){
		return this.noise;
	}
	public String getColor(){
		return this.color;
	}
	public int getPower(){
		return this.power;
	}
}