import java.util.Scanner;
public class ToasterStore{
	
	public static void main (String[] args){
	Scanner sc = new Scanner(System.in);
	
		Toaster[] toasters = new Toaster[4];
		
		for(int i =0; i < toasters.length; i++){
			// toasters[i] = new Toaster("Ding","Black",600);
			 
			System.out.println("What sound would a Toaster make?");
			String toasterNoise = sc.nextLine();
			//toasters[i].setNoise(toasterNoise);
			
			System.out.println("What color is the toaster?");
			String toasterColor = sc.nextLine();
			//toasters[i].setColor(toasterColor);
			
			System.out.println("How strong is your Toaster? (Give a Wattage)");
			int toasterPower = sc.nextInt();
			//toasters[i].setPower(toasterPower);
			
			toasters[i] = new Toaster(toasterNoise, toasterColor, toasterPower);
			sc.nextLine();
			
		}
		
		
		System.out.println("Before calling the setters");
		
		System.out.println(toasters[3].getNoise());
		System.out.println(toasters[3].getColor());
		System.out.println(toasters[3].getPower());
		
		
		System.out.println("Let's change the last Toaster a bit...");
		
		System.out.println("Hmmmm... So what noise would the last toaster actually make?");
		String toasterNoise = sc.nextLine();
			toasters[3].setNoise(toasterNoise);
			
		//System.out.println("Hmmmm... So what color would the last toaster actually is?");
		//String toasterColor = sc.nextLine();
			//toasters[3].setColor(toasterColor);
			// got asked to remove a setter for one of the fields in q1 of part 3 so :p
		
		System.out.println("Hmmmm... So how much power does  the last toaster actually have?");
		int toasterPower = sc.nextInt();
			toasters[3].setPower(toasterPower);
			
		System.out.println("After calling the setters");
			
		System.out.println(toasters[3].getNoise());
		System.out.println(toasters[3].getColor());
		System.out.println(toasters[3].getPower());
			
		System.out.println("Calling the method Powerup");
		
		System.out.println("How much power do you wanna add to your second toaster >:)");
		
		int amountPowerUp = sc.nextInt();	
		
			toasters[1].powerUpValue(amountPowerUp);
			
		System.out.println(toasters[3].getNoise());
		System.out.println(toasters[3].getColor());
		System.out.println(toasters[3].getPower());
		
		toasters[0].ding();
		toasters[0].toast();
		
	}
}